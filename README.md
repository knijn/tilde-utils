# tilde-utils
 
Utilities for tildes and pubnixes

## Included:
* filesize.sh
* homesize.sh
* playremote.sh

## filesize
Takes $1 as an argument for the file, can also be a directory such as `~/Downloads/some-folder/*`

## homesize
Show the file size of the home folder, will also show some information about the folders inside home